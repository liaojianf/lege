import {Body, Controller, Get, Post, Query, Render, Response} from '@nestjs/common';
import {AdminService} from "../admin/admin.service";

@Controller()
export class RegisterController {
    constructor(private readonly adminService: AdminService) {}

    @Get('/toregister')
    @Render("C:\\Users\\ljf\\Desktop\\nest\\nest-demo\\src\\view\\register.ejs")
    toregister(@Query() query){
        if(query.warning != null){
            console.log(query.warning)
            return {warning:false}
        }
        return {warning:true}
    }

    @Post('/register')
   async  register(@Body() body,@Response() res){
       const registers = await this.adminService.register(body);
       console.log(registers);
       if(registers == false){
           res.redirect("/toregister?warning=1");

       }

    }

}
