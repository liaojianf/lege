import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose'
import {StudentSchema} from './schemas/app.schema'
import {AdminSchema} from './schemas/admin.schema'
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { AdminController } from './admin/admin.controller';
import { AdminService } from './admin/admin.service';
import {LoggerMiddleware} from "./logger.middlerware";
import {TestMiddleware} from "./test.middlerware"
import {AuthMiddleware} from "./auth/auth.middleware"
import {LoginMiddleware} from "./auth/login.middleware"
import { FileuploadController } from './fileupload/fileupload.controller';
import { CookieController } from './cookie/cookie.controller';
import { SessionController } from './session/session.controller';
import { ExceptionController } from './exception/exception.controller';
import apply = Reflect.apply;
import {APP_INTERCEPTOR} from "@nestjs/core";
import {LoggingInterceptor} from "./interceptor/logging.interceptor";
import {CacheInterceptor} from "./interceptor/cache.interceptor";
import { AuthModule } from './auth/auth.module';
import { RegisterController } from './register/register.controller';
import { JwtAuthModule } from './jwtauth/jwtauth.module';
import { JwtUsersModule } from './jwtusers/jwtusers.module';
import { JwtController } from './jwt/jwt.controller';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/mongoose_test',{ useUnifiedTopology: true ,useNewUrlParser: true}),
      MongooseModule.forFeature(([{name:'Student',schema:StudentSchema,collection:"students"},
          {name:'Admin',schema:AdminSchema,collection:"admin"}])),
      AuthModule,
],
    controllers: [AppController, AdminController, FileuploadController, CookieController, SessionController, ExceptionController, RegisterController, JwtController],
  providers: [AppService, AdminService
      // ,
      // // 全局拦截器
      // {provide: APP_INTERCEPTOR,useClass:LoggingInterceptor},
      // {provide: APP_INTERCEPTOR,useClass:CacheInterceptor}
      ]
})

export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            // .apply(AuthMiddleware)
            // .forRoutes(AppController)
            .apply(LoginMiddleware)
            .forRoutes(AppController);
    }
}
