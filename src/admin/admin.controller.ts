import {
    Controller, Get, Post, Query, Render, UseFilters, Response, Body, UsePipes, Param,
    UseInterceptors, UseGuards, Request, HttpStatus
} from '@nestjs/common';
import {AdminPipe} from '../admin.pipe'
import {ExcludeNullInterceptor} from '../interceptor/excludeNull.Interceptor'
import {HttpExceptionFilter} from  '../http-exception.filter '
import { AdminService } from './admin.service';
import {RolesGuard} from "../auth/roles.gurad";
import {Roles} from "../Dto/roles.decorator";
import {AuthGuard} from "../auth/auth.guard";
import {LoginException} from "../common/exceptions/login.exception";
import {ApiErrorCode} from "../common/exceptions/enums/api-error-code.enum";

@Controller()
@UseFilters(HttpExceptionFilter)
export class AdminController {
    constructor(private readonly adminService: AdminService) {}
    @Get('/')
    // @UseInterceptors(LoggingInterceptor)
    @Render("C:\\Users\\ljf\\Desktop\\nest\\nest-demo\\src\\view\\login.ejs")
    async tologinUser(){
        console.log("登陆页面Loading");
    }
    @Get('/loginout')
    loginout(@Request() req,@Response() res){
        req.session.role = '';
        req.session.username = '';
        res.redirect('/');

    }


    @Post('/login')
    @UseGuards(AuthGuard)
    @UseInterceptors(ExcludeNullInterceptor)
    @UsePipes(new AdminPipe())
    async loginUser(@Body() body,@Response() rep,@Request() req){
        req.session.username = body.username;
        let boolean  = await this.adminService.login(body);
        // rep.redirect('/failed');
        if(boolean == true){
            console.log("登陆成功");
            rep.redirect('/getUser');
        }else {
            throw new LoginException("用户名或者密码错误", ApiErrorCode.USER_ID_INVALID, HttpStatus.BAD_REQUEST);
        }
    }

    @Get('/failed')
    @Render('C:\\Users\\ljf\\Desktop\\nest\\nest-demo\\src\\view\\failedlogin.ejs')
    failed(){

    }
}
