import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import {AdminService} from "../admin/admin.service";
// import {UserService} from "./user.service";

@Injectable()
export class LoginMiddleware implements NestMiddleware<Request|any, Response> {
    constructor(private readonly adminService: AdminService) {}
    async use(req: Request|any, res: Response, next: Function) {
        const name = req.session.username;
        req.session.page = 0;
        console.log("处理/login请求的中间件..");
        let users = await this.adminService.getUserByName(name);
         if(users.role.includes("p1")){
             req.session.role = "管理员";
         } else {
             req.session.role = "普通用户";
         }
        console.log(users);
        req.user = users;
        next();
    }
}